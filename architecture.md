```mermaid
sequenceDiagram
    autonumber
    Tif Upload Service->>Tif GCP Bucket: Upload tif to bucket
    Tif GCP Bucket-->>GCP Pub Sub: tif uploaded to bucket
    GCP Pub Sub->>Tif to COG Service: tif bucket object name to convert
    Tif to COG Service->>COG GCP Bucket: convert and upload tif as cog
    GCP Pub Sub->>Tif Upload Service: send email to user with cog object url
```

```mermaid
stateDiagram-v2 
    s0 : GeoTiff
    s1 : Unprocessed Storage Bucket
    s2 : PubSub
    s3 : TIF to COG Service
    s4 : Processed Storage Bucket


    s0 --> s1 : Upload
    s1 --> s2 : Publishes 'New Object' Message
    s2 --> s3 : Subscription listening for 'New Object' Message pushes message
    s3 --> s4 : TIF to COG service pulls data from Unprocessed Storage Bucket, converts it to COG, and uploads it
```

