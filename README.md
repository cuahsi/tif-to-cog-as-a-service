# GeoFormat Cloud Converters

A plugable event-driven service ecosystem for converting geospatial data formats to their
cloud-native counterparts.

As our data grows larger, it intrinsically becomes more difficult, time consuming, and expensive to
move, manage, and share. To overcome these and other challenges, the geospatial community has put
forward alternative cloud-native data formats that enable users to work with large datasets without
downloading the entire dataset. This enables scientists and practitioners to work and interact with
_just_ the data they need, rather than the work coming after a full dataset has been downloaded.

## How it works

1. Scientist or practitioner upload their dataset to our portal
2. Our processing pipeline will convert their dataset to a cloud optimized format and,
3. Their cloud-native dataset will be accessible for download, viewing, and sharing from the _Datasets_
portion of the portal.

For now, we are holding onto converted datasets indefinitely. However, be advised, in the future
this will likely change.

## Services

So far, we've completed two services that we consider to have high community reward and a low
barrier for entry. These services convert shapefiles and geotiffs to flatgeobufs and cloud optimized
geotiffs (COGs) respectively. Both of these cloud-native formats work with common tools like QGIS,
GDAL, and Geopandas.

What we have so far:

- [Upload](./services/upload_service/) - frontend web server for Cloud Converter project. Browse and
view converted datasets and upload new datasets for processing.
- [Vector to FGB](./services/gdal-ogr-vector/) - converts shapefiles to flatgeobufs.
- [TIF to COG](./services/gdal-translate-tif/) - converts GeoTIF files to Cloud Optimized GeoTifs.
- [Extract Feature](./services/feature_extractor/) - extracts metadata from a flatgeobuf.
- [Extract Raster](./services/raster_extractor/) - extracts metadata from a COG file.
- [Bucket File Listing](./services/dataset_index_service/) - list bucket files in a predetermined format.

## High-Level Architecture Model

The architecture consists of services that respond to events. For example, when a file is uploaded,
an event is pushed to the appropriate service for handling that upload event. Our services are short
lived  containerized jobs that do one thing and do it well.  More concretely, when
the file has been uploaded, the storage service (in this case GCP Cloud Storage) publishes an event
on a topic. Subscribers to that topic are sent the file uploaded event and act on that information.
You can imagine a service may themselves publish events to the same or another topic during their
processing phase. This enables services to react to a configurable set of events with little to no
business logic intervention.

Our event driven architecture is driven by Google Cloud's PubSub Service and our services are
powered by Google Cloud Run.

For more finer detail, see these [architecture](./architecture.md) diagrams.

## Developer Guide

Developers should work on a singular service at a time. Our deployment relies on a Google Cloud
manager services, and as such, we do not have a way to reproduce the production environment locally.
Thus, making end-to-end testing impossible. At this time, developers are advised to test services by
mocking Google Cloud PubSub messages. An example message can be located [here](./pubsubmessage.md).

Each service lives under a directory in the `services/` folder. Each service _should_ have a
`docker-compose` file to enable building and running the service locally. Additionally, service
specific documentation is located within each service's directory.
