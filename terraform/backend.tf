terraform {
  backend "gcs" {
    bucket = "cuahsi-terraform-state-storage"
    # parent "folder" of tf state file in bucket
    prefix = "tif-to-cog-as-a-service"
  }
}
