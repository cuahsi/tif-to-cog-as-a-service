resource "google_storage_bucket" "unprocessed_storage" {
  project  = var.project
  location = var.location
  name     = "unprocessed_storage"

  force_destroy               = true
  storage_class               = "STANDARD"
  uniform_bucket_level_access = true

  # delete objects that are older than 1 day
  lifecycle_rule {
    action {
      type = "Delete"
    }
    condition {
      # 1 day
      age = 1
    }
  }

  versioning {
    enabled = false
  }

}


resource "google_storage_bucket" "processed_storage" {
  project  = var.project
  location = var.location
  name     = "processed_storage"

  force_destroy               = true
  storage_class               = "STANDARD"
  uniform_bucket_level_access = true

  # lifecycle_rule {
  #   action {
  #     type = "Delete"
  #   }
  #   condition {
  #     # 1 day
  #     age = 1
  #   }
  # }
  cors {
    origin          = ["*"]
    method          = ["GET", "HEAD"]
    response_header = ["Content-Type: x-goog-resumable"]
    max_age_seconds = 3600
  }

  versioning {
    enabled = false
  }

}
