// Services often need their associated service account's secret. These can be retrieved as shown:
// e.g. `gcloud iam service-accounts keys create ./secret.json --iam-account=upload_sa@thredds.iam.gserviceaccount.com`
resource "google_service_account" "upload_sa" {
  project      = var.project
  account_id   = "upload-sa"
  display_name = "Upload Service Account"
  disabled     = false
}

resource "google_storage_bucket_iam_binding" "upload_sa_storage_iam" {
  bucket  = google_storage_bucket.unprocessed_storage.name
  role    = "roles/storage.objectCreator"
  members = ["serviceAccount:${google_service_account.upload_sa.email}", ]
}

# --------------------------------------------------------- #
#                   TIF to COG Service                      #
# --------------------------------------------------------- #

resource "google_service_account" "tif_to_cog_sa" {
  project      = var.project
  account_id   = "tif-to-cog-sa"
  display_name = "Tif to Cog Service Account"
  disabled     = false
}

resource "google_storage_bucket_iam_binding" "tif_to_cog_sa_unprocessed_storage_iam" {
  bucket = google_storage_bucket.unprocessed_storage.name
  role   = "roles/storage.objectViewer"
  members = [
    "serviceAccount:${google_service_account.tif_to_cog_sa.email}",
    "serviceAccount:${google_service_account.vector_to_fgb_sa.email}",
    "allUsers"
  ]
}

resource "google_storage_bucket_iam_binding" "tif_to_cog_sa_processed_storage_iam" {
  bucket = google_storage_bucket.processed_storage.name
  role   = "roles/storage.objectCreator"
  members = [
    "serviceAccount:${google_service_account.tif_to_cog_sa.email}",
    "serviceAccount:${google_service_account.vector_to_fgb_sa.email}",
  ]
}

resource "google_pubsub_topic_iam_binding" "tif_to_cog_sa_pubsub_publisher_iam" {
  project = var.project
  topic   = google_pubsub_topic.upload.id
  role    = "roles/pubsub.subscriber"
  members = [
    "serviceAccount:${google_service_account.tif_to_cog_sa.email}",
    "serviceAccount:${google_service_account.vector_to_fgb_sa.email}"
  ]
}

# --------------------------------------------------------- #
#                   Vector to FGB Service                   #
# --------------------------------------------------------- #

resource "google_service_account" "vector_to_fgb_sa" {
  project      = var.project
  account_id   = "vector-to-fgb-sa"
  display_name = "Vector to fgb Service Account"
  disabled     = false
}

# --------------------------------------------------------- #
#                   Unprocessed storage                     #
# --------------------------------------------------------- #

resource "google_service_account" "unprocessed_storage_sa" {
  project      = var.project
  account_id   = "unprocessed-storage-sa"
  display_name = "Unprocessed storage service account"
  disabled     = false
}

resource "google_storage_bucket_iam_binding" "unprocessed_storage_sa_iam" {
  bucket  = google_storage_bucket.unprocessed_storage.name
  role    = "roles/storage.objectAdmin"
  members = ["serviceAccount:${google_service_account.unprocessed_storage_sa.email}", ]
}

resource "google_pubsub_topic_iam_binding" "unprocessed_storage_sa_pubsub_publisher_iam" {
  project = var.project
  topic   = google_pubsub_topic.upload.id
  role    = "roles/pubsub.publisher"
  # members = ["serviceAccount:${google_service_account.unprocessed_storage_sa.email}", ]
  # pubsub notifications from a gcp bucket are bound to the project level service account agent
  # see: https://cloud.google.com/storage/docs/projects#service-accounts
  members = ["serviceAccount:${data.google_storage_project_service_account.gcs_account.email_address}", ]
}

resource "google_storage_notification" "unprocessed_storage_notifications" {
  # attribute name        example
  # notificationConfig 	  projects/_/buckets/foo/notificationConfigs/3
  # eventType 	          OBJECT_FINALIZE
  # payloadFormat 	      JSON_API_V1
  # bucketId 	            foo
  # objectId 	            bar
  # objectGeneration 	    123456
  # eventTime 	          2021-01-15T01:30:15.01
  bucket         = google_storage_bucket.unprocessed_storage.name
  payload_format = "JSON_API_V1"
  topic          = google_pubsub_topic.upload.id
  event_types    = ["OBJECT_FINALIZE"] # send event when new object has been created

  depends_on = [google_pubsub_topic_iam_binding.unprocessed_storage_sa_pubsub_publisher_iam]
}

# --------------------------------------------------------- #
#                   Processed storage                       #
# --------------------------------------------------------- #

resource "google_service_account" "processed_storage_sa" {
  project      = var.project
  account_id   = "processed-storage-sa"
  display_name = "Processed storage service account"
  disabled     = false
}

resource "google_storage_bucket_iam_binding" "processed_storage_sa_iam" {
  bucket  = google_storage_bucket.processed_storage.name
  role    = "roles/storage.objectAdmin"
  members = ["serviceAccount:${google_service_account.processed_storage_sa.email}", ]
}

resource "google_pubsub_topic_iam_binding" "processed_storage_sa_pubsub_publisher_iam" {
  project = var.project
  topic   = google_pubsub_topic.convert.id
  role    = "roles/pubsub.publisher"
  # pubsub notifications from a gcp bucket are bound to the project level service account agent
  # see: https://cloud.google.com/storage/docs/projects#service-accounts
  members = ["serviceAccount:${data.google_storage_project_service_account.gcs_account.email_address}", ]
}

resource "google_project_iam_binding" "processed_storage_sa_pubsub_token_creator_iam" {
  project = var.project
  role    = "roles/iam.serviceAccountTokenCreator"

  # see: https://cloud.google.com/run/docs/tutorials/pubsub#integrating-pubsub
  # > Important: If your project was created on or before April 8, 2021, you must grant the
  # > iam.serviceAccountTokenCreator role to the Google-managed service account
  # > service-{PROJECT_NUMBER}@gcp-sa-pubsub.iam.gserviceaccount.com on the project in order to allow
  # > Pub/Sub to create tokens. However, if your project was created after that date, you do not need to
  # > grant it this role because it has the roles/pubsub.serviceAgent role with identical permissions.
  members = [
    "serviceAccount:${data.google_storage_project_service_account.gcs_account.email_address}",
    "serviceAccount:${google_project_service_identity.pubsub_sa.email}"
  ]
}


resource "google_storage_notification" "processed_storage_notifications" {
  # attribute name        example
  # notificationConfig 	  projects/_/buckets/foo/notificationConfigs/3
  # eventType 	          OBJECT_FINALIZE
  # payloadFormat 	      JSON_API_V1
  # bucketId 	            foo
  # objectId 	            bar
  # objectGeneration 	    123456
  # eventTime 	          2021-01-15T01:30:15.01
  bucket         = google_storage_bucket.processed_storage.name
  payload_format = "JSON_API_V1"
  topic          = google_pubsub_topic.convert.id
  event_types    = ["OBJECT_FINALIZE"] # send event when new object has been created

  depends_on = [google_pubsub_topic_iam_binding.processed_storage_sa_pubsub_publisher_iam]
}

# --------------------------------------------------------- #
#                   tif to cog PubSub push                  #
# --------------------------------------------------------- #

resource "google_service_account" "tif_to_cog_pubsub_sa" {
  project      = var.project
  account_id   = "tif-to-cog-pubsub-sa"
  display_name = "tif to cog pubsub service account"
  disabled     = false
}

# --------------------------------------------------------- #
#                   vector to fgb PubSub push               #
# --------------------------------------------------------- #

resource "google_service_account" "vector_to_fgb_pubsub_sa" {
  project      = var.project
  account_id   = "vector-to-fgb-pubsub-sa"
  display_name = "vector to fgb pubsub service account"
  disabled     = false
}

# --------------------------------------------------------- #
#                   PubSub sa setup.                        #
#    required b.c. project created before April 8, 2021     #
# --------------------------------------------------------- #


# Only needed for projects created on or before April 8, 2021:
# Grant the Google-managed service account the `iam.serviceAccountTokenCreator` role
# "roles/iam.serviceAccountTokenCreator"
# "service-718580702159@gcp-sa-pubsub.iam.gserviceaccount.com"

# pubsub cannot deliver messages without this

data "google_iam_policy" "admin" {
  binding {
    role = "roles/iam.serviceAccountTokenCreator"
    members = [
      # "serviceAccount:service-718580702159@gcp-sa-pubsub.iam.gserviceaccount.com",
      "service-718580702159@gcp-sa-pubsub.iam.gserviceaccount.com",
    ]
  }
}
