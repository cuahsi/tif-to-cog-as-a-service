resource "google_cloud_run_service" "upload_service" {
  name     = "upload-service"
  project  = var.project
  location = var.location

  template {
    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale" = "10"
      }
    }
    spec {
      service_account_name = google_service_account.upload_sa.email

      container_concurrency = 100
      timeout_seconds       = 900
      containers {
        image = "us-central1-docker.pkg.dev/thredds/cloud-native-format-adapters/upload-service:0.0.2"

        env {
          name  = "BUCKET_NAME"
          value = google_storage_bucket.unprocessed_storage.name
        }
        env {
          name  = "DATASET_BUCKET_NAME"
          value = google_storage_bucket.processed_storage.name
        }

        resources {
          limits = {
            cpu    = "1000m"
            memory = "128Mi"
          }
        }
        ports {
          name           = "http1"
          container_port = 8000
          protocol       = "TCP"
        }
      }
    }
  }
}

resource "google_cloud_run_service_iam_binding" "upload_service_sa_binding" {
  project  = var.project
  location = var.location

  service = google_cloud_run_service.upload_service.name
  role    = "roles/run.invoker"
  members = [
    "allUsers"
  ]
}
