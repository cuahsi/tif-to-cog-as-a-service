# project level service account agent
# see: https://cloud.google.com/storage/docs/projects#service-accounts
data "google_storage_project_service_account" "gcs_account" {
  project = var.project
}
