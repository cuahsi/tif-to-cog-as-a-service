# retrieve service-{PROJECT_NUMBER}@gcp-sa-pubsub.iam.gserviceaccount.com service account
# needed because:
# see: https://cloud.google.com/run/docs/tutorials/pubsub#integrating-pubsub
# > Important: If your project was created on or before April 8, 2021, you must grant the
# > iam.serviceAccountTokenCreator role to the Google-managed service account
# > service-{PROJECT_NUMBER}@gcp-sa-pubsub.iam.gserviceaccount.com on the project in order to allow
# > Pub/Sub to create tokens. However, if your project was created after that date, you do not need to
# > grant it this role because it has the roles/pubsub.serviceAgent role with identical permissions.
resource "google_project_service_identity" "pubsub_sa" {
  provider = google-beta

  project = var.project
  service = "pubsub.googleapis.com"
}
