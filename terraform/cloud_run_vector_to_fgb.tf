resource "google_cloud_run_service" "vector_to_fgb_service" {
  name     = "vector-to-fgb-service"
  project  = var.project
  location = var.location

  template {
    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale" = "10"
      }
    }
    spec {
      service_account_name = google_service_account.vector_to_fgb_sa.email

      container_concurrency = 10
      # 15 min timeout. this may need to be increased as the size of files grow larger.
      timeout_seconds = 900

      volumes {
        name = "secrets"
        secret {
          secret_name  = "tif-to-cog-sa"
          default_mode = 256 # 0400
          items {
            key  = "1"
            path = "secret.json"
          }
        }

      }
      containers {
        image = "us-central1-docker.pkg.dev/thredds/cloud-native-format-adapters/vector-to-fgb-service:0.0.1a1"

        volume_mounts {
          name       = "secrets"
          mount_path = "/run/secrets"
        }
        env {
          // GOOGLE_APPLICATION_CREDENTIALS needed by gdal. see: https://gdal.org/user/virtual_file_systems.html#vsigs-google-cloud-storage-files
          name  = "GOOGLE_APPLICATION_CREDENTIALS"
          value = "/run/secrets/secret.json"
        }
        env {
          name  = "OUTPUT_BUCKET"
          value = google_storage_bucket.processed_storage.name
        }

        resources {
          limits = {
            cpu    = "2000m"
            memory = "4G"
          }
        }
        ports {
          name           = "http1"
          container_port = 8000
          protocol       = "TCP"
        }
      }
    }
  }
}


resource "google_cloud_run_service_iam_binding" "vector_to_fgb_service_sa_binding" {
  project  = var.project
  location = var.location

  service = google_cloud_run_service.vector_to_fgb_service.name
  role    = "roles/run.invoker"
  members = [
    "serviceAccount:${google_service_account.vector_to_fgb_pubsub_sa.email}",
    "allUsers"
  ]
}

// TODO: in the future, use `google_secret_manager_secret_iam_binding` instead. this will allow
// services to share a terraform reference name.
resource "google_secret_manager_secret_iam_member" "vector_to_fgb_sa_secret" {
  project   = var.project
  secret_id = "projects/thredds/secrets/tif-to-cog-sa"
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:${google_service_account.vector_to_fgb_sa.email}"
}
