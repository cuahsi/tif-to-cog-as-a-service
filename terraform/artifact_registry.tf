resource "google_artifact_registry_repository" "image-repository" {
  project  = var.project
  location = var.location

  provider = google-beta

  repository_id = "cloud-native-format-adapters"
  description   = "image repo containing cloud native format adapter services"
  format        = "DOCKER"
}
