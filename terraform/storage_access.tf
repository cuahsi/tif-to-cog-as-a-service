resource "google_storage_bucket_iam_member" "unprocessed_storage_public_access" {
  bucket = google_storage_bucket.unprocessed_storage.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}

resource "google_storage_bucket_iam_member" "processed_storage_public_access" {
  bucket = google_storage_bucket.processed_storage.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}
