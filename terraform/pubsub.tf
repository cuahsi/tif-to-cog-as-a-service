resource "google_pubsub_topic" "upload" {
  project                    = var.project
  name                       = "upload"
  message_retention_duration = "86400s" # 1 day
}

resource "google_pubsub_topic" "convert" {
  project                    = var.project
  name                       = "convert"
  message_retention_duration = "86400s" # 1 day
}

resource "google_pubsub_subscription" "tif_to_cog_unprocessed_storage_pubsub" {
  project = var.project
  # data uploaded to unprocessed storage
  name  = "data-uploaded-to-unprocessed-storage"
  topic = google_pubsub_topic.upload.name

  ack_deadline_seconds = 300

  filter = "hasPrefix(attributes.objectId, \"tif/\")"
  # "tif/south_carolina/something.tif" # would match prefix"tif"

  push_config {
    push_endpoint = "https://tif-to-cog-service-edqqnq2wjq-uc.a.run.app/" # TODO: Add tif-to-cog service endpoint
    oidc_token {
      service_account_email = google_service_account.tif_to_cog_pubsub_sa.email
    }

    attributes = {
      x-goog-version = "v1"
    }
  }
}

resource "google_pubsub_subscription" "vec_to_fgb_unprocessed_storage_pubsub" {
  project = var.project
  # data uploaded to unprocessed storage
  name  = "shp-uploaded-to-unprocessed-storage"
  topic = google_pubsub_topic.upload.name

  ack_deadline_seconds = 300 // this should probably be the lifetime of a container. maybe with an added buffer?

  filter = "hasPrefix(attributes.objectId, \"shp/\")"

  push_config {
    push_endpoint = "https://vector-to-fgb-service-edqqnq2wjq-uc.a.run.app"
    oidc_token {
      service_account_email = google_service_account.vector_to_fgb_pubsub_sa.email
    }

    attributes = {
      x-goog-version = "v1"
    }
  }
}
