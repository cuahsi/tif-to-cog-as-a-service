variable "project" {
  type        = string
  description = "gcp project"
}

variable "location" {
  type        = string
  default     = "us-central1"
  description = "gcp location"
}
