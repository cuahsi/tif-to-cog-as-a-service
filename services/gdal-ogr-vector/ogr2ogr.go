package main

import (
	"context"
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/lukeroth/gdal"

	"gitlab.com/cuahsi/tif-to-cog-as-a-service/services/gdal-ogr-vector/models"
)

// verify acceptable MIME type
type GDALVirtualFsMimeMapping struct {
	ContentType       string
	VirtualFileSystem string
}

// these need to be sorted by `ContentType` to enable binary search
type GDALVirtualFsMimeMappings []*GDALVirtualFsMimeMapping

type Config struct {
	output_bucket string
	port          string
	dev           bool
}

// these need to be sorted by `ContentType` to enable binary search
var ACCEPTED_TYPES GDALVirtualFsMimeMappings = GDALVirtualFsMimeMappings{
	&GDALVirtualFsMimeMapping{ContentType: "application/gzip", VirtualFileSystem: "vsigzip"},
	&GDALVirtualFsMimeMapping{ContentType: "application/zip", VirtualFileSystem: "vsizip"},
}

func (m *GDALVirtualFsMimeMappings) String() string {
	var s []byte
	delimiter := []byte(", ")

	a := *m
	l := len(a)

	if l > 0 {
		last_idx := len(a) - 1

		for _, item := range a[:last_idx] {
			str := append([]byte(item.ContentType), delimiter...)
			s = append(s, str...)
		}
		// dont include delimiter for last item of slice
		s = append(s, []byte(a[last_idx].ContentType)...)
	}

	return string(s)
}

// assumes receiver, m *GDALVirtualFsMimeMappings, is sorted ascending by ContentType
func (m *GDALVirtualFsMimeMappings) Search(needle string) (*GDALVirtualFsMimeMapping, error) {
	a := *m

	i := sort.Search(len(a), func(i int) bool { return a[i].ContentType >= needle })

	if i < len(a) && a[i].ContentType == needle {
		// is present
		found := a[i]
		return found, nil
	}

	return nil, fmt.Errorf("needle, %s, not found", needle)
}

// general procedure
// pubsub pushes data to cloud run function
// marshall data from ps to struct
// extract `bucketId` and `objectId` fields from `message.attributes`

var config Config

func getEnv(env string) string {
	e := os.Getenv(env)
	if e == "" {
		panic(fmt.Sprintf("Env var, %s, not set.", env))
	}
	return e
}

func init() {
	port := getEnv("PORT")
	dev := os.Getenv("DEV")
	b, _ := strconv.ParseBool(dev)

	var bucket string

	// OUTPUT_BUCKET env var not parsed in dev mode
	if !b {
		bucket = getEnv("OUTPUT_BUCKET")
	}

	// global configuration object
	config = Config{
		output_bucket: bucket,
		port:          port,
		dev:           b,
	}
}

// uploadFile uploads an object.
func uploadFile(bucket_name, object_name string, r io.Reader) error {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("storage.NewClient: %v", err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(ctx, time.Second*50)
	defer cancel()

	o := client.Bucket(bucket_name).Object(object_name)

	// do not upload if the object already exists in bucket
	o = o.If(storage.Conditions{DoesNotExist: true})

	// Upload an object with storage.Writer.
	wc := o.NewWriter(ctx)
	if _, err = io.Copy(wc, r); err != nil {
		return fmt.Errorf("io.Copy: %v", err)
	}
	if err := wc.Close(); err != nil {
		return fmt.Errorf("Writer.Close: %v", err)
	}
	return nil
}

func vectorTranslate(c *gin.Context) {
	var msg models.Message // on creation, all fields are their nil type. so, if type is string, then field == "" at creation
	err := c.ShouldBindJSON(&msg)

	if err != nil {
		// ignore missing BucketId field in DEV mode
		if config.dev && strings.Contains(err.Error(), "BucketId") {
			//noop
		} else {
			log.Printf("%+v", msg)
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
	}

	decoded_data, err := base64.StdEncoding.DecodeString(msg.Message.Data)

	if err != nil {
		log.Printf("%+v", msg)
		c.JSON(http.StatusBadRequest, gin.H{"Error": fmt.Sprintf("could not decode base64 message attribute data. %s", err.Error())})
		return
	}

	var data models.MessageData
	err = binding.JSON.BindBody(decoded_data, &data)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Error": fmt.Sprintf("could not bind decoded data to struct of type %T. %s", data, err.Error())})
		return
	}

	vsi, err := ACCEPTED_TYPES.Search(strings.ToLower(data.ContentType))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Error": fmt.Sprintf("unsupported MIME type: %s. supported MIME types: %s", data.ContentType, &ACCEPTED_TYPES)})
		return
	}

	input_file_name := fmt.Sprintf("/%s/vsigs_streaming/%s/%s", vsi.VirtualFileSystem, msg.Message.Attributes.BucketId, msg.Message.Attributes.ObjectId)

	if config.dev && msg.Message.Attributes.BucketId == "" {
		input_file_name = fmt.Sprintf("/%s/%s", vsi.VirtualFileSystem, msg.Message.Attributes.ObjectId)
	}

	log.Printf("input file name: %s", input_file_name)
	srcDs, err := gdal.OpenEx(input_file_name, gdal.OFReadOnly, nil, nil, nil)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Error": err.Error()})
		return
	}

	defer srcDs.Close()

	basename := filepath.Base(msg.Message.Attributes.ObjectId)
	extension := filepath.Ext(basename)
	output_file_name := fmt.Sprintf("%s.fgb", basename[:len(basename)-len(extension)])
	log.Printf("output file name: %s", output_file_name)

	// NOTE: uses OS specific temp dir location. See `os.CreateTemp` docs for implementation assumptions
	dir, err := os.MkdirTemp("", basename)

	// remove directory later if not in DEV mode
	if !config.dev {
		defer os.RemoveAll(dir)
	}

	if err != nil {
		log.Println("temp dir creation failed")
		c.JSON(http.StatusExpectationFailed, gin.H{"error": fmt.Sprintf("failed translating file %q from Vector to FlatGeoBuf", msg.Message.Attributes.ObjectId)})
		return
	}

	converted_file_path := filepath.Join(dir, output_file_name)

	// NOTE: using gdal's in memory file system support (MEM:::) probably makes the most sense when deploying this service
	// however, the go ("github.com/lukeroth/gdal") gdal bindings do no provide a very _go-like_ interface (e.g. io.Reader, io.Writer).
	// so for now, writing to a temp file seems like the best way around this.
	output_file, err := gdal.VectorTranslate(converted_file_path, []gdal.Dataset{srcDs}, []string{"-f", "FlatGeoBuf"})

	if err != nil {
		c.JSON(http.StatusExpectationFailed, gin.H{"error": fmt.Sprintf("failed translating file %q from Vector to FlatGeoBuf", msg.Message.Attributes.ObjectId)})
		return
	}
	output_file.Close()

	// don't attempt upload
	if config.dev {
		c.JSON(http.StatusOK, gin.H{"status": "it worked", "output_path": converted_file_path})
		return
	}

	f, err := os.Open(converted_file_path)
	if err != nil {
		log.Printf("converted file %q failed to open", converted_file_path)
		c.JSON(http.StatusExpectationFailed, gin.H{"error": err.Error()})
		return
	}
	defer f.Close()

	if err != nil {
		c.JSON(http.StatusExpectationFailed, err.Error())
		return
	}

	err = uploadFile(config.output_bucket, output_file_name, f)

	if err != nil {
		c.JSON(http.StatusExpectationFailed, err.Error())
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "it worked"})
}

func main() {
	r := gin.Default()
	r.POST("/", vectorTranslate)
	port := fmt.Sprintf(":%s", config.port)
	r.Run(port)
}
