Environment variables:

At runtime:

- `PORT` : http server port.
- `OUTPUT_BUCKET` (required if `DEV` is unset or false) : Bucket files are written to.
- `GOOGLE_APPLICATION_CREDENTIALS` : location inside container where gcp service account credentials (`json` formatted) are mounted.
- `DEV` (not required) : true to enable development mode.
  - files are not uploaded to `OUTPUT_BUCKET`.
  - set `message.attributes.bucketId` to `""` in message to operate on local files.

At compile time:

- `GIN_MODE` : `debug`, `release`, `test` default, `debug`
