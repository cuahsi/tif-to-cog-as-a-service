package models

type Message struct {
	Message MessageBody `json:"message" binding:"required"`
	// MessageId string `json:"messageId"`
	// PublishTime
}

type MessageBody struct {
	Attributes Attributes `json:"attributes"`
	Data       string     `json:"data" binding:"required"`
}

type Attributes struct {
	BucketId           string `json:"bucketId" binding:"required"`
	EventTime          string `json:"eventTime"`
	EventType          string `json:"eventType"`
	NotificationConfig string `json:"notificationConfig"`
	ObjectGeneration   string `json:"objectGeneration"`
	ObjectId           string `json:"objectId" binding:"required"`
	PayloadFormat      string `json:"payloadFormat"`
}

type MessageData struct {
	Kind                    string `json:"kind"`
	Id                      string `json:"id"`
	SelfLink                string `json:"selfLink"`
	Name                    string `json:"name"`
	Bucket                  string `json:"bucket"`
	Generation              string `json:"generation"`
	Metageneration          string `json:"metageneration"`
	ContentType             string `json:"contentType" binding:"required"`
	TimeCreated             string `json:"timeCreated"`
	Updated                 string `json:"updated"`
	StorageClass            string `json:"storageClass"`
	TimeStorageClassUpdated string `json:"timeStorageClassUpdated"`
	Size                    string `json:"size"`
	Md5Hash                 string `json:"md5Hash"`
	MediaLink               string `json:"mediaLink"`
	ContentLanguage         string `json:"contentLanguage"`
	Crc32c                  string `json:"crc32c"`
	Etag                    string `json:"etag"`
}
