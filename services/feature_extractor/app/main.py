import json
from google.cloud import storage
from fastapi import FastAPI
from app.models import Message
from app.feature_extractor import extract_metadata

app = FastAPI()


@app.post("/metadata")
def extract_metadata_endpoint(message: Message):
    client = storage.Client()

    message = message.message
    filename = message.attributes.objectId
    file_path = f"/vsigs_streaming/{message.attributes.bucketId}/{message.attributes.objectId}"
    json_metadata = extract_metadata(file_path)
    json_metadata_str = json.dumps(json_metadata, indent=2)

    bucket = client.bucket("processed_storage")
    blob = bucket.blob(f'{filename}.hs_meta.json')

    blob.upload_from_string(json_metadata_str)
    return json_metadata
