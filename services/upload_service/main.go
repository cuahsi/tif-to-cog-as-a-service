package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"strconv"
)

type Config struct {
	bucket_name         string
	dataset_bucket_name string
	port                uint16
}

type Token struct {
	Token string `json:"token"`
}

type Bucket struct {
	BucketName string
}

const TEMPLATE_DIR = "templates/*"

var (
	config    = Config{}
	templates = template.Must(template.ParseGlob(TEMPLATE_DIR))
	logger    = log.Default()
)

func getEnvVariable(name string) string {
	value := os.Getenv(name)
	if value == "" {
		error_message := fmt.Sprintf("env variable, %s was not set.", name)
		panic(error_message)
	}
	return value
}

func init() {
	bn := getEnvVariable("BUCKET_NAME")
	ds_bn := getEnvVariable("DATASET_BUCKET_NAME")
	port := getEnvVariable("PORT")
	port_n, err := strconv.Atoi(port)

	if err != nil || port_n < 0 {
		panic(fmt.Sprintf("could not convert port, %s, to a uint16.\n", port))
	}

	uport_n := uint16(port_n)

	// set config
	config.bucket_name = bn
	config.dataset_bucket_name = ds_bn
	config.port = uport_n
}

func setupServer() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/token", tokenHandler)
	http.HandleFunc("/datasets", datasetsHandler)
	http.HandleFunc("/rasterViewer", rasterViewer)
	http.HandleFunc("/vectorViewer", vectorViewer)

	port := fmt.Sprintf(":%d", config.port)

	logger.Printf("serving on %s\n", port)

	http.ListenAndServe(port, nil)
	fmt.Printf("%v\n", config)

}

func main() {
	setupServer()
}

func RenderTemplate(w http.ResponseWriter, name string, data any) {
	t := templates.Lookup(name)

	if t == nil {
		http.Error(w, fmt.Sprintf("template, %s, not found.", name), http.StatusInternalServerError)
		return
	}

	if err := t.Execute(w, data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}
