Environment variables:

- `BUCKET_NAME`
- `DATASET_BUCKET_NAME`
- `PORT`
- `GOOGLE_APPLICATION_CREDENTIALS` : required for local development
- `GIN_MODE`
