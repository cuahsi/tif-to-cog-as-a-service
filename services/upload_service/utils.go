package main

import (
	"context"
	"fmt"
	"time"

	"cloud.google.com/go/storage"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

func AccessTokenFromSecretFile(scope ...string) (string, error) {
	// oauth2 scopes that might be required:
	// https://www.googleapis.com/auth/devstorage.read_write
	// sa1 creates jwt
	// create oauth token for
	// https://cloud.google.com/iam/docs/create-short-lived-credentials-direct
	// POST https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/SA_NAME@PROJECT_ID.iam.gserviceaccount.com:generateAccessToken
	ctx := context.Background()
	data, err := google.FindDefaultCredentials(ctx, scope...)
	if err != nil {
		return "", err
	}

	token, err := data.TokenSource.Token()
	if err != nil {
		return "", err
	}
	return token.AccessToken, nil
}

func listFiles(bucket string, opts ...option.ClientOption) ([]*storage.ObjectAttrs, error) {
	ctx := context.Background()

	client, err := storage.NewClient(ctx, opts...)
	var o []*storage.ObjectAttrs

	if err != nil {
		return o, fmt.Errorf("storage.NewClient: %v", err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()

	it := client.Bucket(bucket).Objects(ctx, nil)
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return o, fmt.Errorf("Bucket(%q).Objects: %v", bucket, err)
		}
		o = append(o, attrs)
	}
	return o, nil
}
