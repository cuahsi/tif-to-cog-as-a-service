package main

import (
	"flag"
	"fmt"
	"html/template"
	"os"
	"strings"
)

const TEMPLATE_DIR = "templates/*"

func main() {
	// -t template name
	// -r templates root
	// -o output file
	tmpl := flag.String("t", "", "template name")
	tmpl_dir := flag.String("d", "", "template directory")
	output_file := flag.String("o", "", "rendered template output file name")
	input_data := flag.String("i", "", "input data specified as key=value,key2=value")

	flag.Parse()

	if *tmpl == "" || *tmpl_dir == "" || *output_file == "" {
		flag.PrintDefaults()
		return

	}
	data := make(map[string]string)

	if len(*input_data) > 0 {
		split := strings.Split(*input_data, ",")

		for _, pair := range split {
			idx := strings.Index(pair, "=")

			if idx == -1 {
				continue
			}
			value := pair[idx+1:]
			fmt.Printf("%q\n", value)
			data[pair[:idx]] = value
		}
	}

	*tmpl_dir = strings.TrimSuffix(*tmpl_dir, "/")
	templates := template.Must(template.ParseGlob(fmt.Sprintf("%s/*", *tmpl_dir)))
	t := templates.Lookup(*tmpl)

	if t == nil {
		fmt.Printf("template, %s, not found in template dir %s", *tmpl, *tmpl_dir)
		return
	}

	f, err := os.Create(*output_file)
	if err != nil {
		fmt.Printf("could not create file, %s", *output_file)
		return
	}
	defer f.Close()

	if err := t.Execute(f, data); err != nil {
		fmt.Printf("could execute template, %s", *tmpl)
		return
	}
}
