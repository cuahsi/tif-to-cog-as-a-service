package main

type Dataset struct {
	Name string
	Type string
	URI  string
}

type Datasets []Dataset
