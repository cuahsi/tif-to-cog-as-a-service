#!/bin/bash

export BUCKET_NAME=unprocessed_storage
export DATASET_BUCKET_NAME=processed_storage
export PORT=8000
export SECRET_FILE=secret.json

go run .
