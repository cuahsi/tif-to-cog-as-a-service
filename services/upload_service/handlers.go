package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"strings"

	"google.golang.org/api/option"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		RenderTemplate(w, "404.html", r.URL.Path)
		return
	}
	bucket_info := Bucket{BucketName: config.bucket_name}
	RenderTemplate(w, "index.html", bucket_info)
}

func tokenHandler(w http.ResponseWriter, r *http.Request) {
	scope := "https://www.googleapis.com/auth/devstorage.read_write"
	token, err := AccessTokenFromSecretFile(scope)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	t := Token{Token: token}

	json.NewEncoder(w).Encode(&t)
}

func datasetsHandler(w http.ResponseWriter, r *http.Request) {
	o, err := listFiles(config.dataset_bucket_name, option.WithoutAuthentication())
	if err != nil {
		log.Printf("failed to retrieve object metadata from %q. error: %s", config.dataset_bucket_name, err.Error())
		RenderTemplate(w, "502.html", nil)
	}

	var data Datasets
	for _, item := range o {
		name := filepath.Base(item.Name)
		var _type string
		var viewer_url string

		// TODO: move this logic somewhere else and improve it's implementation
		switch {
		case strings.HasSuffix(name, "tif"):
			_type = "COG"
			u := fmt.Sprintf("gs://%s/%s", item.Bucket, item.Name)
			viewer_url = fmt.Sprintf("/rasterViewer?url=%s", u)
		case strings.HasSuffix(name, "fgb"):
			_type = "FGB"
			u := fmt.Sprintf("https://storage.googleapis.com/%s/%s", item.Bucket, item.Name)
			viewer_url = fmt.Sprintf("/vectorViewer?url=%s", u)
		}

		if _type == "" {
			continue
		}
		data = append(data, Dataset{
			Name: name,
			Type: _type,
			URI:  viewer_url,
		})
	}

	RenderTemplate(w, "datasets.html", data)
}

func viewerHandler(template_name string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		url, ok := r.URL.Query()["url"]
		if !ok || len(url) < 1 {
			log.Println("query parameter `url` not provided")
			RenderTemplate(w, "404.html", nil)
			return
		}
		RenderTemplate(w, template_name, struct{ Url string }{url[0]})
	}
}

var rasterViewer = viewerHandler("raster_viewer.html")
var vectorViewer = viewerHandler("vector_viewer.html")
