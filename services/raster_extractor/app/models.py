from pydantic import BaseModel


class Attributes(BaseModel):
	bucketId: str
	eventTime: str
	eventType: str
	notificationConfig: str
	objectGeneration: str
	objectId: str
	payloadFormat: str


class MessageBody(BaseModel):
	attributes: Attributes


class Message(BaseModel):
	message: MessageBody
