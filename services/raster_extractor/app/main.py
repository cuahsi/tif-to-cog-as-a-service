from google.cloud import storage
from fastapi import FastAPI
from app.models import Message
from app.raster_meta_extract import extract_from_tif_file

app = FastAPI()


@app.post("/metadata")
def extract_metadata(message: Message):
    message = message.message
    filename = message.attributes.objectId
    file_path = f"/vsigs_streaming/{message.attributes.bucketId}/{message.attributes.objectId}"
    json_metadata_str = extract_from_tif_file(file_path)

    client = storage.Client()
    bucket = client.bucket("processed_storage")
    blob = bucket.blob(f'{filename}.hs_meta.json')

    blob.upload_from_string(json_metadata_str)
    return json_metadata_str
