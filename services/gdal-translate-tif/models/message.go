package models

type Message struct {
	Message MessageBody `json:"message" binding:"required"`
	// Data string `json:"data"`
	// MessageId string `json:"messageId"`
	// PublishTime
}

type MessageBody struct {
	Attributes Attributes `json:"attributes"`
}

type Attributes struct {
	BucketId           string `json:"bucketId" binding:"required"`
	EventTime          string `json:"eventTime"`
	EventType          string `json:"eventType"`
	NotificationConfig string `json:"notificationConfig"`
	ObjectGeneration   string `json:"objectGeneration"`
	ObjectId           string `json:"objectId" binding:"required"`
	PayloadFormat      string `json:"payloadFormat"`
}
