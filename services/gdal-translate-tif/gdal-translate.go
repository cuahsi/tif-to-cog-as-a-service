package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"cloud.google.com/go/storage"
	"github.com/gin-gonic/gin"
	"github.com/lukeroth/gdal"

	"gitlab.com/cuahsi/tif-to-cog-as-a-service/gdal-translate-tif/models"
)

type Config struct {
	output_bucket string
	port          string
}

// general procedure
// pubsub pushes data to cloud run function
// marshall data from ps to struct
// extract `bucketId` and `objectId` fields from `message.attributes`

var config Config

func getEnv(env string) string {
	e := os.Getenv(env)
	if e == "" {
		panic(fmt.Sprintf("Env var, %s, not set.", e))
	}
	return e
}

func init() {
	bucket := getEnv("OUTPUT_BUCKET")
	port := getEnv("PORT")

	// global configuration object
	config = Config{
		output_bucket: bucket,
		port:          port,
	}
}

// uploadFile uploads an object.
func uploadFile(bucket_name, object_name string, r io.Reader) error {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("storage.NewClient: %v", err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(ctx, time.Second*50)
	defer cancel()

	o := client.Bucket(bucket_name).Object(object_name)

	// do not upload if the object already exists in bucket
	o = o.If(storage.Conditions{DoesNotExist: true})

	// Upload an object with storage.Writer.
	wc := o.NewWriter(ctx)
	if _, err = io.Copy(wc, r); err != nil {
		return fmt.Errorf("io.Copy: %v", err)
	}
	if err := wc.Close(); err != nil {
		return fmt.Errorf("Writer.Close: %v", err)
	}
	return nil
}

func Translate(c *gin.Context) {
	var json models.Message // on creation, all fields are their nil type. so, if type is string, then field == "" at creation
	err := c.ShouldBindJSON(&json)

	if err != nil {
		log.Printf("message binding failed: %+v", &json)
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	input_file_name := fmt.Sprintf("/vsigs_streaming/%s/%s", json.Message.Attributes.BucketId, json.Message.Attributes.ObjectId)
	input_file, err := gdal.Open(input_file_name, 0)
	defer input_file.Close()

	if err != nil {
		log.Printf("gdal open failed for filename: %s", input_file_name)
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	basename := filepath.Base(json.Message.Attributes.ObjectId)
	ext := filepath.Ext(basename)
	basename_no_ext := basename[:len(basename)-len(ext)]
	output_file_name := fmt.Sprintf("%s.tif", basename_no_ext)

	tmp, err := os.MkdirTemp("", output_file_name)

	output_file_path := filepath.Join(tmp, output_file_name)

	// remove directory later if not in DEV mode
	defer os.RemoveAll(tmp)

	if err != nil {
		log.Println("temp dir creation failed")
		c.JSON(http.StatusExpectationFailed, gin.H{"error": fmt.Sprintf("failed translating file %q from TIF to COG", json.Message.Attributes.ObjectId)})
		return
	}

	output_file, err := gdal.Translate(output_file_path, input_file, []string{"-of", "COG", "-co", "COMPRESS=LZW"})
	output_file.Close()

	if err != nil {
		c.JSON(http.StatusExpectationFailed, gin.H{"error": fmt.Sprintf("failed translating file %q from TIF to COG", input_file_name)})
		return
	}

	f, err := os.Open(output_file_path)
	if err != nil {
		log.Printf("converted file %q failed to open", output_file_path)
		c.JSON(http.StatusExpectationFailed, gin.H{"error": err.Error()})
		return
	}
	defer f.Close()

	err = uploadFile(config.output_bucket, output_file_name, f)

	if err != nil {
		c.JSON(http.StatusExpectationFailed, err.Error())
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "it worked"})
}

func main() {
	r := gin.Default()
	r.POST("/", Translate)
	port := fmt.Sprintf(":%s", config.port)
	r.Run(port)
}
