Environment variables:

At runtime:

- `PORT` : http server port
- `OUTPUT_BUCKET` : Bucket files are written to.
- `GOOGLE_APPLICATION_CREDENTIALS` : location inside container where gcp service account credentials (`json` formatted) are mounted.

At compile time:

- `GIN_MODE` : `debug`, `release`, `test` default, `debug`
