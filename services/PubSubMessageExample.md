GCP PubSub Message Example:

```json
{
    "message": {
        "attributes": {
            "bucketId": "unprocessed_storage",
            "eventTime": "2022-06-09T18:39:14.630149Z",
            "eventType": "OBJECT_FINALIZE",
            "notificationConfig": "projects/_/buckets/unprocessed_storage/notificationConfigs/9",
            "objectGeneration": "1654799954618669",
            "objectId": "031001.tif",
            "payloadFormat": "JSON_API_V1"
        },
        "data": "ewogICJraW5kIjogInN0b3JhZ2Ujb2JqZWN0IiwKICAiaWQiOiAidW5wcm9jZXNzZWRfc3RvcmFnZS8wMzEwMDEudGlmLzE2NTQ3OTk5NTQ2MTg2NjkiLAogICJzZWxmTGluayI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9zdG9yYWdlL3YxL2IvdW5wcm9jZXNzZWRfc3RvcmFnZS9vLzAzMTAwMS50aWYiLAogICJuYW1lIjogIjAzMTAwMS50aWYiLAogICJidWNrZXQiOiAidW5wcm9jZXNzZWRfc3RvcmFnZSIsCiAgImdlbmVyYXRpb24iOiAiMTY1NDc5OTk1NDYxODY2OSIsCiAgIm1ldGFnZW5lcmF0aW9uIjogIjEiLAogICJjb250ZW50VHlwZSI6ICJpbWFnZS90aWZmIiwKICAidGltZUNyZWF0ZWQiOiAiMjAyMi0wNi0wOVQxODozOToxNC42MzBaIiwKICAidXBkYXRlZCI6ICIyMDIyLTA2LTA5VDE4OjM5OjE0LjYzMFoiLAogICJzdG9yYWdlQ2xhc3MiOiAiU1RBTkRBUkQiLAogICJ0aW1lU3RvcmFnZUNsYXNzVXBkYXRlZCI6ICIyMDIyLTA2LTA5VDE4OjM5OjE0LjYzMFoiLAogICJzaXplIjogIjkzMjAwNTIiLAogICJtZDVIYXNoIjogIlFLcjhDczk1c2hFR2NHYVFuUHp2VUE9PSIsCiAgIm1lZGlhTGluayI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9kb3dubG9hZC9zdG9yYWdlL3YxL2IvdW5wcm9jZXNzZWRfc3RvcmFnZS9vLzAzMTAwMS50aWY/Z2VuZXJhdGlvbj0xNjU0Nzk5OTU0NjE4NjY5JmFsdD1tZWRpYSIsCiAgImNvbnRlbnRMYW5ndWFnZSI6ICJlbiIsCiAgImNyYzMyYyI6ICJpY0szTlE9PSIsCiAgImV0YWciOiAiQ0szUzRKdUNvZmdDRUFFPSIKfQo=",
        "messageId": "4828445215264270",
        "message_id": "4828445215264270",
        "publishTime": "2022-06-09T18:39:14.778Z",
        "publish_time": "2022-06-09T18:39:14.778Z"
    },
    "subscription": "projects/thredds/subscriptions/data-uploaded-to-unprocessed-storage"
}
```

Note that the `message.data` key is base64 encoded.

Decoded utf-8 string representation:

```json
{
  "kind": "storage#object",
  "id": "unprocessed_storage/031001.tif/1654799954618669",
  "selfLink": "https://www.googleapis.com/storage/v1/b/unprocessed_storage/o/031001.tif",
  "name": "031001.tif",
  "bucket": "unprocessed_storage",
  "generation": "1654799954618669",
  "metageneration": "1",
  "contentType": "image/tiff",
  "timeCreated": "2022-06-09T18:39:14.630Z",
  "updated": "2022-06-09T18:39:14.630Z",
  "storageClass": "STANDARD",
  "timeStorageClassUpdated": "2022-06-09T18:39:14.630Z",
  "size": "9320052",
  "md5Hash": "QKr8Cs95shEGcGaQnPzvUA==",
  "mediaLink": "https://www.googleapis.com/download/storage/v1/b/unprocessed_storage/o/031001.tif?generation=1654799954618669&alt=media",
  "contentLanguage": "en",
  "crc32c": "icK3NQ==",
  "etag": "CK3S4JuCofgCEAE="
}
```

Test the service locally:


```shell
curl -X POST -H "content-type: application/json" \
--data '{
    "message": {
        "attributes": {
            "bucketId": "unprocessed_storage",
            "eventTime": "2022-06-09T18:39:14.630149Z",
            "eventType": "OBJECT_FINALIZE",
            "notificationConfig": "projects/_/buckets/unprocessed_storage/notificationConfigs/9",
            "objectGeneration": "1654799954618669",
            "objectId": "031001.tif",
            "payloadFormat": "JSON_API_V1"
        },
        "data": "ewogICJraW5kIjogInN0b3JhZ2Ujb2JqZWN0IiwKICAiaWQiOiAidW5wcm9jZXNzZWRfc3RvcmFnZS8wMzEwMDEudGlmLzE2NTQ3OTk5NTQ2MTg2NjkiLAogICJzZWxmTGluayI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9zdG9yYWdlL3YxL2IvdW5wcm9jZXNzZWRfc3RvcmFnZS9vLzAzMTAwMS50aWYiLAogICJuYW1lIjogIjAzMTAwMS50aWYiLAogICJidWNrZXQiOiAidW5wcm9jZXNzZWRfc3RvcmFnZSIsCiAgImdlbmVyYXRpb24iOiAiMTY1NDc5OTk1NDYxODY2OSIsCiAgIm1ldGFnZW5lcmF0aW9uIjogIjEiLAogICJjb250ZW50VHlwZSI6ICJpbWFnZS90aWZmIiwKICAidGltZUNyZWF0ZWQiOiAiMjAyMi0wNi0wOVQxODozOToxNC42MzBaIiwKICAidXBkYXRlZCI6ICIyMDIyLTA2LTA5VDE4OjM5OjE0LjYzMFoiLAogICJzdG9yYWdlQ2xhc3MiOiAiU1RBTkRBUkQiLAogICJ0aW1lU3RvcmFnZUNsYXNzVXBkYXRlZCI6ICIyMDIyLTA2LTA5VDE4OjM5OjE0LjYzMFoiLAogICJzaXplIjogIjkzMjAwNTIiLAogICJtZDVIYXNoIjogIlFLcjhDczk1c2hFR2NHYVFuUHp2VUE9PSIsCiAgIm1lZGlhTGluayI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9kb3dubG9hZC9zdG9yYWdlL3YxL2IvdW5wcm9jZXNzZWRfc3RvcmFnZS9vLzAzMTAwMS50aWY/Z2VuZXJhdGlvbj0xNjU0Nzk5OTU0NjE4NjY5JmFsdD1tZWRpYSIsCiAgImNvbnRlbnRMYW5ndWFnZSI6ICJlbiIsCiAgImNyYzMyYyI6ICJpY0szTlE9PSIsCiAgImV0YWciOiAiQ0szUzRKdUNvZmdDRUFFPSIKfQo=",
        "messageId": "4828445215264270",
        "message_id": "4828445215264270",
        "publishTime": "2022-06-09T18:39:14.778Z",
        "publish_time": "2022-06-09T18:39:14.778Z"
    },
    "subscription": "projects/thredds/subscriptions/data-uploaded-to-unprocessed-storage"
}' https://tif-to-cog-service-edqqnq2wjq-uc.a.run.app
# }' localhost:8080/translate
```
